# webphone

__SayCel Webphone - An App for Community Cell Networks__


___

## What is this project about?
Webphone is a project that aimed to create seamless communication between Community Cellular Networks with anyone 
outside those networks with an internet connection and the Webphone app. Check the [webphone site](http://webphone.saycel.com/) for more info.
The main project development stopped in spring 2018. Given the large amount of work that went it to it, the current developer is making an attempt to complete some of
the functionality.

## Current Development and problems a.k.a "challenges":

* The main network controllers seem currently unsure about PWAs or so-called "Progressive Web Apps" - for the moment, support on iOS devices 
for anything other than the most basic interaction with the system seems minimal - No background audio for example, being something of a show stopper here.
On other platforms, it's also unclear what is happening. Access may or may not be available to the internal microphone and speaker on a "phone" device, and this may 
depend on versions, flags given to versions at runtime on desktop, or settings changed from defaults in about:config or chrome://flags These things may change at any time, so we are subjected to 
breakage by user upgrades of the browser.

* The UX logic relies too much on push notifications which currently makes the program only really viable on the "chrome" browser. As per that products controlling corporation's apparent policy of moving more and more 
functionality into the "play services" and thus control away from the user, latest versions of chrome are now insisting that the "play" services be installed 
on the device. Again, this is something of a show stopper for a community network tool. Also, these "push" services won't work without a connection to the corporate internet.

* It is maybe possible that the "PWA" can be wrapped inside a "package" and therefore installed as an "app". On iOS there is something called Cordova. 
It then runs in a web view (or something) spawned by the app. It's unclear to me at this time if this will relax any controls on device access.


* It's written in angularjs. - This is a massive project, The official documentation leans towards the technical and tends to be confusing. Concise, clear examples are lacking and many changes across versions adds to the confusion. Add to the mix a plethora of dependencies and dependencies of dependencies and it becomes an on-boarding nightmare.  
* The are some ~2,500 lines of TypeScript in the project, most of which is in the SIP handling and the user/database management, angular compiles this to some **250,000** lines of javascript that I have not audited, nor am I going to audit.
* A lot of the intended functionality for the end user experience relies on push notifications, therefore on centralised services. 
* Controlling the use of the internal speaker from a so-called "PWA" seems unwieldy.
* Some browsers do not currently support actions in push notifications, nor do there appear to be any concrete plans at this time for implementation. This may be a good thing, given that searching about them reveals a disturbing amount of guides and howtos on their use for user nudging and behaviour modification.
* angular and dependencies seem to loose backward compatibility quickly with older browsers, forcing updates, in some cases therefore forcing OS and associated hardware upgrade. There may be work arounds. (pros and cons of running older browsers aside)


That said, current development has mainly a desktop platform in mind. This should still be useful. Personally my feeling for mobile is that we could have written, or even just made a customised version of such as CSIPSimple by now.


## How does it work?
From the perspective of the people in the Community Cellular Networks, they will simply call or text anyone outside the network having the Webphone app 
in the same way that they do with the people inside the network. 
The people outside will be able to talk or text anyone in the communities by using the webphone app. 
Find more information in the [project's presentation at FOSDEM 2018](https://fosdem.org/2018/schedule/event/webphone/)

## Which Community Cellular Networks will be benefited?
The first targeted communities were those supported by Rhizomatica in México and SayCel in Nicaragua, the organizations that initially promoted Webphone. 
For more information about these organizations and the communities that they serve, check:

● https://www.rhizomatica.org/

● www.saycel.com

## How to contribute
If you want to contribute, please read the CONTRIBUTING.md file.

## What is this repo?
In this repository you will find the source code of the progressive web app (PWA) part of the Webphone system.
The PWA can communicate with the Community Network server, request a virtual phone number, store contacts, initiate and receive calls. 
Also if you access from an Android device with a supporting browser you can install it as if it were a native application.

## It's not..

This app is the frontend of a more complex system, with it alone you can not mount a virutal telephone system. 
It acts as a client for the user and connects with notifications and WebRTC communication services via JsSip.
The other components are avaliable here:

● https://dev.rhizomatica.org:8888/keith/wp-kamailio

● https://dev.rhizomatica.org:8888/keith/webpush

## Parts that make up the app
This development is done on Angular. Use:
* Angular Material for interface components
* jsSip for SIP and webrtc (control plane and audio)
* webpush-api developed to manage notifications between Kamailio and the PWA
* pouchDb for local data storage (contact, settings, user data)

## Development environment
The recommended development environment was:
* Node 7.5.0 (original author recommended installation with [nvm](https://github.com/creationix/nvm)) 
* Current Author's note: I really do not know enough about Node et al to make any recommendations.
* Clone and install this repository
* Run `npm install` after each fetch or pull in which the package.json file changes

### Variables and configurations
There are two places that centralize the possible configurations of the app: __src/app/jssip.config.ts__ and the files inside the folder __src/environments__

#### jssip.config.ts
Most of them respond to the standard configuration of jsSip (you can see the information on your website), the "custom" elements are this:
```javascript
{
    ...
    custom:
        {
            
            // If another address is used as a gateway for calls. If you do not use leave in null
            dtmfsGateway: '385485876@did.callwithus.com',

            // If you use an outbound key for area codes. If you do not use it, leave it in null.
            outbound: null,

            // Domain to add to calls that have none.
            defaultUtiDomain: 'rhizortc.specialstories.org',

            // Array of special prefix codes to identify when the call is to a virutal number.
            virtualNumbersPrefixs: [999100, 999200],

            // Virtual number prefix to request when a user is created
            virtualNumberPrefix: 999111,

            // Array of prefixes for conference calls.
            conferenceCallPrefixs: [500],

            // Fake domain for the automatic request of new numbers.
            fakeEmail: '@ generic_email.saycel'
        }
}
```

#### Environment Variables
The environment variables are managed by Angular when making the build or raising a development server. In the case of a production build Angular takes as valid the options entered in `src / environments / environment.prod.ts`.
Actually there are two fields to use:
* endpoint -> The url of the webpush-server service
* kamailioNewNumber -> The url of the service that assigns new numbers.

