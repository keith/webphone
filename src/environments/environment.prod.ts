export const environment = {
  production: true,
  endpoint: 'https://webphone.rhizomatica.org/webpush/',
  kamailioNewNumber: 'https://saycel.specialstories.org/cgi-bin/allocatenumber.py',
  URIDomain: 'rhizortc.specialstories.org',
  socket: {
	uri           : 'wss://rhizortc.specialstories.org:8443',
	via_transport : 'auto',
  },
};
