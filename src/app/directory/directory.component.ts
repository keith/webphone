import { Component, OnInit, OnDestroy, ChangeDetectorRef } from '@angular/core';
import { group, trigger, state, animate, transition, style, query, animateChild } from '@angular/animations';
import { Router } from '@angular/router';
import { DirectoryService, DirectoryI, DirectoryItemI } from '../directory.service';
import { StorageService } from '../storage.service';
import { UserService, UserI } from '../user.service';

import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import 'rxjs/add/operator/takeUntil';

@Component({
  selector: 'app-directory',
  templateUrl: './directory.component.html',
  styleUrls: ['./directory.component.scss'],
  animations: [
    trigger('toggleState', [
      state('true' , style({ maxHeight: '100%' })),
      state('false', style({ maxHeight: '38px', backgroundColor: 'grey' })),
      transition('* => *', [
        group([
          query('.arrow', animateChild()),
          animate('400ms')
        ])
      ])
    ]),
    trigger('arrowState', [
      state('true' , style({ transform: 'rotate(0)'})),
      state('false', style({ transform: 'rotate(-90deg)'})),
      transition('* => *', animate('150ms'))
    ])
  ],
})

export class DirectoryComponent implements OnInit, OnDestroy {

  public directories: Observable<DirectoryI[]>;
  public contacts: Observable<DirectoryItemI[]>;
  public contactsToggle = true;
  public directoryToggle = true;
  public user: UserI;
  private ngUnsubscribe = new Subject<void>();

  constructor(
    private _router: Router,
    public directoryService: DirectoryService,
    public storageService: StorageService,
    public userService: UserService,
    private cd: ChangeDetectorRef
  ) {

    console.debug('[DIRECTORY] is constructing');
    /* Get an Observable.of the DirectoryI */
    this.directories = directoryService.get();

    storageService.table('user').info().catch((error) => {
      this.user = {};
      console.warn('[DIR] (constructor) ' + error.reason);
      throw error;
    });

    this.contacts = storageService
      .table('contacts')
      .read()
      .takeUntil(this.ngUnsubscribe);

  }

  ngOnInit() {
    this.userService
    .userData()
    .takeUntil(this.ngUnsubscribe)
    .subscribe(
        x => {
          this.user = x;
          console.debug('[DIRECTORY] sub, user', this.user);
          this.cd.detectChanges();
    });
  }

  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }

  call(item: DirectoryItemI) {
    this._router.navigate(['/call', item.number]);
  }

  add(contact: DirectoryItemI) {
    this.storageService.table('contacts').create(contact);
  }

  edit(contact: DirectoryItemI) {
    this._router.navigate(['/directory', 'edit', contact._id]);
  }

  sms(contact: DirectoryItemI) {
    this._router.navigate(['/chat', 'conversation', contact.number]);
  }

  toggleContactsList(value: boolean) {
    this.contactsToggle = !this.contactsToggle;
  }

  toggleDirectoryList(value: boolean) {
    this.directoryToggle = !this.directoryToggle;
  }
}
