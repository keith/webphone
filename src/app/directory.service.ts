import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import 'rxjs/add/observable/of';

export interface DirectoryItemI {
  number: number;
  title: string;
  subtitle?: string;
  _id?: string;
}
export interface DirectoryI {
  title: string;
  items: DirectoryItemI[];
}

@Injectable()
/**
 * Simple service, only get directory array from directory.json
 */
export class DirectoryService {
  obs: Observable<DirectoryI[]>;
  directoryLoaded: DirectoryI[];

  constructor(_http: HttpClient) {
      this.obs = _http.get('directory.json')
      .do(res => this.directoryLoaded = res as any)
      .do(console.log);
  }

  get(): Observable<DirectoryI[]> {
    return (typeof this.directoryLoaded !== 'undefined') ? Observable.of(this.directoryLoaded) : this.obs;
  }
}
