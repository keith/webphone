import { BrowserModule, Title } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { Routes, RouterModule, PreloadAllModules } from '@angular/router';
import { ServiceWorkerModule } from '@angular/service-worker';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CustomMaterialModule } from './material.module';

import { AppComponent } from './app.component';

import { CallModule } from './call/call.module';
import { DirectoryModule } from './directory/directory.module';
import { ChatModule } from './chat/chat.module';
import { TestModule } from './test/test.module';

import { ToneService } from './tone.service';
import { JsSipService } from './jssip.service';
import { DirectoryService } from './directory.service';
import { StorageService } from './storage.service';
import { UserService } from './user.service';
import { CallStatusComponent } from './call-status/call-status.component';
import { CallSurveyComponent } from './call-survey/call-survey.component';
import { CallSurveyService } from './call-survey.service';
import { MessageBoxComponent } from './message-box/message-box.component';
import { GuiNotificationsService } from './gui-notifications.service';
import { SmsService } from './sms.service';
import { AboutComponent } from './about/about.component';
import { AccountComponent } from './account/account.component';

export const appRoutes: Routes  = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'call'
  },
  {
    path: 'call',
    loadChildren: () => import('./call/call.module').then(m => m.CallModule)
  },
  {
    path: 'chat',
    loadChildren: () => import('./chat/chat.module').then(m => m.ChatModule)
  },
  {
    path: 'test',
    loadChildren: () => import('./test/test.module').then(m => m.TestModule)
  },
  {
    path: 'about',
    component: AboutComponent
  },
  {
    path: 'account',
    component: AccountComponent
  },
  {
    path: '**',
    redirectTo: '/call',
    pathMatch: 'full'
  },
];

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
  declarations: [
    AppComponent,
    CallStatusComponent,
    CallSurveyComponent,
    MessageBoxComponent,
    AboutComponent,
    AccountComponent
  ],
  imports: [
    RouterModule.forRoot(
      appRoutes,
      {
        preloadingStrategy: PreloadAllModules,
        useHash: true
      }
    ),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
        }
    }),
    ServiceWorkerModule.register('/ngsw-worker.js'),
    CustomMaterialModule,
    BrowserAnimationsModule,
    BrowserModule,
    HttpClientModule,
    CallModule,
    ChatModule,
    DirectoryModule,
    TestModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [
    DirectoryService,
    CallSurveyService,
    GuiNotificationsService,
    SmsService,
    Title
  ],
  exports: [TranslateModule],
  bootstrap: [AppComponent]
})
export class AppModule { }
