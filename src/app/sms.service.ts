import { Injectable } from '@angular/core';

import { BehaviorSubject } from 'rxjs/BehaviorSubject';

import { JsSipService } from './jssip.service';
import { UserService} from './user.service';
import { StorageService } from './storage.service';

import { GuiNotificationsService } from './gui-notifications.service';
export enum MessageType {
  TEXT = <any>'text'
}

export interface ChatMessageI {
  _id?: string;
  chatId?: string;
  from?: string;
  content?: string;
  createdAt?: number;
  type?: MessageType;
}

export interface ChatListItemI {
  _id?: string;
  from?: string;
  lastMessage?: ChatMessageI;
  unread?: boolean;
}

interface ConversationI {
  messages?: ChatMessageI[];
  to?: string;
  chatId?: string;
  myNumber?: string;
}

@Injectable()
export class SmsService {
  private _chatsList = new BehaviorSubject<ChatListItemI[]>([]);
  private _chatConversations = new BehaviorSubject<ConversationI[]>([]);
  private myNumber;

  constructor(
    private _jsSipService: JsSipService,
    private _user: UserService,
    private _notifications: GuiNotificationsService,
    public _storage: StorageService,
  ) {

    this._user.isUser().then( status => {
      this._user.userData().subscribe(u => this.myNumber = u.user);
      this._chatConversations.next([]);
      this._chatsList.next([]);

      /* Fill the chat list from items in the local storage. */
      this._storage.table('sms').read().forEach((data) => {
          data.forEach((sms) => {
            this.addSms(sms, sms.chatId);
          });
        })
      .then(console.debug('[SMS] - Init OK'))
      .catch(err => console.debug('[SMS] - Init error', err));
    });

    // Link with jsSip service
    this._jsSipService.incomingSms.subscribe( (data: any) => {

      if (!data) {
        return;
      }

      const checkOfflineInContent = (rowSms) => {
        console.log('[SMS] - Row message', rowSms.request.body, rowSms);
        if (rowSms.request.body.split('[Offline message').length === 1) {
          return rowSms;
        }
        // Remove "[Offline message - " from message contnet
        let content = rowSms.request.body.replace('[Offline message - ', '');
        const smsDate = new Date(content.split(']')[0]);
        smsDate.setTime(smsDate.getTime() - smsDate.getTimezoneOffset() * 60000);
        content = content.split(']')[1];
        rowSms.request.body = content;
        rowSms.message.createdAt = smsDate;
        return rowSms;
      };

      if ( data && data.originator !== 'local') {
        data = checkOfflineInContent(data);
        const message: ChatMessageI = {
          //_id: '' + Date.now(),
          _id: data.request.call_id,
          chatId: data.message.remote_identity.uri.user,
          from: data.message.remote_identity.uri.user,
          content: data.request.body,
          createdAt: data.message.createdAt || Date.now()
        };
        this._storage.table('sms').create(message)
        this.addSms(message, message.chatId);
      }
    });
  }

  sendSms(message: string, to: string) {
    const from = this.myNumber;
    const newMessage: ChatMessageI = {
      _id: '' + Date.now(),
      chatId: to,
      from: from,
      content: message,
      createdAt: Date.now()
    };
    this._jsSipService.sendMessage(message, to)
      .then(data => {
          this.addSms(newMessage, to)
          this._storage.table('sms').create(newMessage)
        })
      .catch(error => {
        this._notifications.send({text: error.cause || error.error });
        console.log('[SMS] - Error sending message', error);
      });
  }

  addSms(message: ChatMessageI, chatId: string) {

    // Check if chat exist, if not create one
    const chats = this._chatsList.getValue();
    if (chats.filter(chat => chat._id === chatId).length === 0) {
      this.initChat(chatId);
    }

    // Change last message on chat list
    this._chatsList.next(this._chatsList.getValue().map(chat => {
      if (chat._id !== chatId)  { return chat; }
      chat.lastMessage = message;
      chat.unread = true;
      return chat;
    }));

    // Check if conversation exist, if not create one
    const conversations = this._chatConversations.getValue();
    if (conversations.filter(conversation => conversation.chatId === chatId).length === 0) {
        this.initConversation(chatId, this.myNumber);
    }

    // Add to converstions list
    this._chatConversations.next(this._chatConversations.getValue().map(conversation => {
      if (conversation.chatId !== chatId)  { return conversation; }
      conversation.messages = conversation.messages.concat([message]);
      conversation.messages.sort((a, b) => a.createdAt - b.createdAt );
      return conversation;
    }));

  }

  initChat(chatId: string) {
    const newChatItem: ChatListItemI = {
      _id: chatId,
      from: chatId,
      lastMessage: {
        content: ''
      },
      unread: false
    };
    this._chatsList.next(this._chatsList.getValue().concat([newChatItem]));
  }

  initConversation(chatId: string, myNumber: string, to: string = null) {
    if (!myNumber) {
      return;
    }
    const newConversationItem: ConversationI = {
      chatId: chatId,
      myNumber: myNumber,
      to: to || chatId,
      messages: [],
    };
    this._chatConversations.next(this._chatConversations.getValue().concat([newConversationItem]));
  }

  getAllChats() {
    return this._chatsList.asObservable();
  }

  getChat(chatId: string) {
    const filtredChat = new BehaviorSubject<ConversationI>({});

    this._chatConversations.subscribe( conversations => {
      const conversation = conversations
        .filter(x => x.chatId === chatId)
        .reduce((p, a) => a, {});
      filtredChat.next(conversation);
    });

    this.markAsRead(chatId);
    return filtredChat;
  }

  markAsRead(chatId) {
    const newChatList = this._chatsList.getValue().map(chat => {
      if (chat._id !== chatId) {
        return chat;
      }
      chat.unread = false;
      return chat;
    });
    this._chatsList.next(newChatList);
  }
}
