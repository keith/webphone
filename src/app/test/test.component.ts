import { Component, OnInit } from '@angular/core';
import { ToneService } from '../tone.service';
import au from '../sounds.service';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.scss'],
  providers: [ ToneService ]
})
export class TestComponent {

  public devices;

  constructor(
  	public ts: ToneService,
  	) {

    console.debug("TEST!", ts, au);

  }

  ngOnInit() {
    console.debug("TEST!");
  }

  showSounds() {
  	document.querySelectorAll('audio').forEach((e) => {
      e.controls = true;
      e.muted = false;
      e.volume = 1.0;
    });
  }

  hideSounds() {
  	document.querySelectorAll('audio').forEach((e) => {
      e.controls = false;
      e.muted = true;
      e.volume = 0;
    } );
  }

  switchAudio() {
    navigator.mediaDevices.getUserMedia( { audio: { deviceId: undefined } } );
    navigator.mediaDevices.enumerateDevices().then((d) => {
      this.devices = d;
      for (let i = 0; i !== this.devices.length; ++i) {
        console.log(this.devices[i]);
        if (this.devices[i].kind == "audiooutput") {
          let a = document.querySelectorAll('audio');
          a.forEach((e) => {
            //let c = <HTMLMediaElement>e;
            let c = <any>e;
            c.setSinkId(this.devices[i].deviceId);
          })
        }
      }
    });
  }

}