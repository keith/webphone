import { Component } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';
import { SwUpdate, SwPush } from '@angular/service-worker';

import { TranslateService } from '@ngx-translate/core';

import { JsSipService } from './jssip.service';
import { DirectoryService, DirectoryItemI } from './directory.service';
import { UserService, UserI } from './user.service';
import { CallSurveyService } from './call-survey.service';
import { GuiNotificationsService } from './gui-notifications.service';
import { versions } from '../environments/versions';

import { DomSanitizer, Title } from '@angular/platform-browser';
import { MatIconRegistry } from '@angular/material';

import { StorageService } from './storage.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  /** This object manages the top navigation bar. */
  public links: any[] = [];

  constructor(
    public iconRegistry: MatIconRegistry,
    public sanitizer: DomSanitizer,
    public directoryService: DirectoryService,
    private userService: UserService,
    public jsSip: JsSipService,
    public storageService: StorageService,
    public callSurveyService: CallSurveyService,
    public notificationsGui: GuiNotificationsService,
    public titleService: Title,
    private route: ActivatedRoute,
    private router: Router,
    private translate: TranslateService,
    private http: HttpClient,
    public update: SwUpdate,
    public push: SwPush,
   ) {

    this.translate.addLangs(['en', 'es', 'pt']);
    const currentLanguage = this.translate.getBrowserLang();
    this.translate.setDefaultLang('en');
    this.translate.use(currentLanguage);

    this.links = [
      { label: 'Call', link: '/call' },
      { label: 'Chat', link: '/chat' },
      { label: 'Directory', link: '/directory' },
    ];

    this.translate.get(['nav.call', 'nav.chat', 'nav.directory']).subscribe((t) => {
        this.links = [
          { label: t['nav.call'], link: '/call' },
          { label: t['nav.chat'], link: '/chat' },
          { label: t['nav.directory'], link: '/directory' },
        ];
      });
    this.translate.get('app.title').subscribe((t) => {
        this.titleService.setTitle(t);
    })

    this.checkVersion();
    this.loadUser();
    this.loadPush();
    this.loadIcons([
      'call-end',
      'call',
      'contact-add',
      'arrow-down',
      'person',
      'star-full',
      'star-border',
      'sms',
      'close'
    ]);

    if (!window.isSecureContext) {
      setTimeout(() => this.notificationsGui.send(
        { text: 'Cannot work without HTTPS',
          timeout: 3000000,
          ti: '🐙'
        }),
      1500);
    }

    if ('serviceWorker' in navigator) {
      // Listen for autoanswer and autoreject messages
      navigator.serviceWorker.addEventListener('message', x => {
        if ( typeof x.data.autoanswer !== 'undefined' && x.data.autoanswer === true) {
          setTimeout(() => this.jsSip.handleAnswerIncoming(), 2000);
        }
        if ( typeof x.data.autoreject !== 'undefined' && x.data.autoreject === true) {
          this.jsSip.handleRejectIncoming();
        }
      });
    }

  }

  /**
   * Check Version.
   */
  checkVersion() {
    if (this.update.isEnabled) {
            this.update.available.subscribe(() => {
                this.translate.get('new_version').subscribe((t) => {
                  if (confirm(t)) {
                    window.location.reload();
                  }
                });
            });
        }
  }

  /**
   * Load svg files into material-icons
   * @param icons  Array of svg file names to load, without the extension
   */
  loadIcons (icons: string[]) {
    icons.forEach( icon =>
      this.iconRegistry
        .addSvgIcon(
          icon,
          this.sanitizer.bypassSecurityTrustResourceUrl('assets/' + icon + '.svg')
        )
    );
  }

  /**
   * Initialize the user system.
   * Load the local database and try to recover the user's data.
   * If they do not exist try to create one automatically.
   */
  loadUser () {
    let connected = false;
    /** subscribe to the user data service */
    this.userService.userData().subscribe(() => {
        this.userService.isUser().then( status => {
          // ** Waiting for the service to be ready.

          if (status === false) {

            /** If the database is fully loaded and there is no user data */
            console.warn('[USER] - No User Data.');
            /** Register user and wait for new user data */
            this.firstTimeMessage();
            this.userService.createUser()
              .catch((error) => console.warn('[USER] ' + error.reason));
          } else if (status === true) {
            /** If the database is fully loaded and there is user data
             *  or if the user data has changed.
             *  Start the jsSip connection */
            this.jsSip.connect(this.userService.userData().getValue())
          }
        })
        .catch( err => console.error('Error in isUser: ', err));
      }
    );
  }

  loadPush() {
    this.userService.isUser().then((status) => {
        if ( status ===  true  ) {
          if ('serviceWorker' in navigator && 'PushManager' in window) {
            console.log('[SW] - Subscription to Push');
            this.push.subscription.subscribe((sub) => {
              if (sub) {
                    console.log('[SW] - Already subscribed', sub.toJSON());
                  } else {
                    console.log('[SW] - No Subscription to PUSH');
                    this.userService.subscribeToPush(this.userService.userData().getValue());
                  }
            });
          } // endif have serviceWorker
        } // endif status
    }).catch( x => console.error('[PUSH] - User Error'));
  }

  firstTimeMessage() {
    if (navigator.userAgent.match(/Android/i) && navigator.userAgent.match(/Mobile/i)) {
      setTimeout(() => this.notificationsGui.send({text: `Your browser may not automatically
      offer you the option to add a shortcut to this application. You can do it manually by
      selecting the options in the upper right corner, and then in the "Add to home screen"`,
      timeout: 10000}), 12000);
    }
  }

}
