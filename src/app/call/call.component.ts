import { Component, HostListener, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { Title } from '@angular/platform-browser';
import { TranslateService } from '@ngx-translate/core';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/takeUntil';

import { ToneService } from '../tone.service';
import { JsSipService } from '../jssip.service';
import { StorageService } from '../storage.service';
import { DirectoryItemI } from '../directory.service';
import { UserService } from '../user.service';

import { versions } from '../../environments/versions';

@Component({
  selector: 'app-call',
  templateUrl: './call.component.html',
  styleUrls: ['./call.component.scss']
})
export class CallComponent implements OnInit, OnDestroy {
  public capable: boolean = false;
  number: string = '';
  contacts: DirectoryItemI[] = [];
  private ngUnsubscribe: Subject<void> = new Subject<void>(); // = new Subject(); in Typescript 2.2-2.4

  constructor(
    private toneService: ToneService,
    public jsSip: JsSipService,
    private route: ActivatedRoute,
    private router: Router,
    public storageService: StorageService,
    public userService: UserService,
    public translate: TranslateService,
    public titleService: Title,
  ) {
    this.userService.isUser().then((status: boolean) => {
     this.capable = status && ('mediaDevices' in navigator);
    });

    storageService.table('contacts')
      .read()
      .takeUntil(this.ngUnsubscribe)
      .subscribe(contacts => {
        this.contacts = contacts;
      });
   }

  ngOnInit() {
    this.number = this.route.snapshot.paramMap.get('number') || '';
    if (this.route.snapshot.paramMap.get('answer') === 'true') {
        this.jsSip.setState({ autoanswer: true });
        this.router.navigate(['/call']);
    }
  }

  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }


  existContact(number) {
    if (typeof(this.contacts) == 'undefined') return false;
    return this.contacts.filter(contact => Number(contact.number) === Number(number)).length > 0;
  }

  addContact(number: string) {
    this.router.navigate(['/directory', 'add', number]);
  }

  @HostListener('document:keydown', ['$event'])
  handleKeyboardEvent(event: KeyboardEvent) {
    const reg = new RegExp('^[0-9\*#]');
    if (reg.test(event.key)) {
      this.pushItem(event.key);
    }
    else if (event.key === 'Enter') {
      this.call();
    }
    else if (event.keyCode === 8 && this.number.length > 0) {
      this.number = this.number.slice(0, -1);
    }
  }

  touchItem(e) {
    this.toneService.start(e);
    this.number += e;
  }

  pushItem(e) {
    if (('ontouchstart' in window) || (navigator.maxTouchPoints > 0) ||
        (navigator.msMaxTouchPoints > 0)) {
      return;
    }
    this.toneService.start(e);
    this.number += e;
  }

  clean() {
    this.number = this.number.substring(0, this.number.length - 1);
  }

  call() {
    if ( this.number === '000000') {
      this.number = versions.branch + ' - ' + versions.revision;
      return;
    }
    if ( this.number === '*1') {
      this.router.navigate(['/about']);
      return;
    }
    const ret = this.jsSip.handleOutgoingCall('', this.number);
    if (!ret) {
      console.log('[CALL error]', ret);
    }
  }

  hangup() {
    if (this.jsSip.state.incomingSession == null &&
          this.jsSip.state.session == null) {
      this.number = '';
    } else {
      this.jsSip.handleHangup();
    }
  }

  takeIncomming() {
    this.jsSip.handleAnswerIncoming();
  }

  hangupIncomming() {
    this.jsSip.handleRejectIncoming();
  }

  toggleReg() {
    if (this.jsSip.getState('status') != 'registered') {
      this.jsSip.register();
    } else {
      this.jsSip.unregister();
    }
  }

}
