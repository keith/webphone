import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TranslateModule } from '@ngx-translate/core';

import { CustomMaterialModule } from '../material.module';
import { CustomsPipesModule } from '../customs-pipes/customs-pipes.module';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ChatConversationComponent } from './chat-conversation/chat-conversation.component';

import { ChatRoutingModule } from './chat-routing.module';
import { ChatListComponent } from './chat-list/chat-list.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    ChatRoutingModule,
    CustomMaterialModule,
    CustomsPipesModule,
    TranslateModule
  ],
  declarations: [ChatConversationComponent, ChatListComponent]
})
export class ChatModule { }
