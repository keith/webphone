import { Component, OnInit, HostListener, ElementRef, OnDestroy, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/takeUntil';

import { JsSipService } from '../../jssip.service';
import { SmsService } from '../../sms.service';
import { ChatMessageI } from '../chat-list/chat-list.component';

interface ConversationI {
  messages: ChatMessageI[];
  to?: string;
  chatId?: string;
  myNumber: string;
}

@Component({
  selector: 'app-chat-conversation',
  templateUrl: './chat-conversation.component.html',
  styleUrls: ['./chat-conversation.component.scss']
})
export class ChatConversationComponent implements OnInit, OnDestroy {

  @ViewChild('conversation', { read: ElementRef } as any) public conversation: ElementRef;
  private ngUnsubscribe: Subject<void> = new Subject<void>();
  public chat;
  public chatId;
  public fixed = false;
  public top: number;

  public message = '';

  constructor(
    private el: ElementRef,
    private _smsService: SmsService,
    private _route: ActivatedRoute,
    private _jsSip: JsSipService,
  ) {}

  ngOnInit() {
    this.chatId = this._route.snapshot.paramMap.get('id') || '';
    this._smsService
      .getChat(this.chatId)
      .takeUntil(this.ngUnsubscribe)
      .subscribe( x => {
        this.chat = x;
        this.scrollOnMessage();
        this._smsService.markAsRead(x.chatId);
      });
    console.log('[SMS] - Chat in list', this.chat);
    this.top = this.el.nativeElement.parentElement.offsetTop - this.el.nativeElement.offsetTop;
    // Nasty hack. this.conversation is undefined when we are called. It comes into being some other time.
    // Like right after we exit? I'm sure this should happen some other place anyway.
    setTimeout(() => this.scrollOnMessage(), 100);
  }

  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }

  scrollOnMessage() {
    if (this.conversation) {
      // We should scroll to the bottom plus the height of the new message.
      // Seems like we get called before the div is on the view.
      setTimeout(
        () => {
          const h = this.conversation.nativeElement.scrollHeight;
          console.log('[SMS] - ScrollHeight: ' + h);
          this.conversation.nativeElement.scroll(0, h);
        },
        100
      );
    }
  }

  @HostListener('window:scroll', [])
  onWindowScroll() {
      console.log(window.scrollY + this.top);
      this.fixed = ((window.scrollY + this.top) > 0);
  }

  @HostListener('document:keydown', ['$event'])
  handleKeyboardEvent(event: KeyboardEvent) {
    if (event.key === 'Enter') {
      event.preventDefault();
      if (this.message != null && this.message !== '') {
        this.send();
      }
    }
  }

  grow(event) {
    const element = event.target;
    element.style.height = '15px';
    element.style.overflowY = 'hidden';
    element.style.height = (element.scrollHeight - 20) + 'px';
    if (element.scrollHeight > 80) {
      element.style.overflowY = 'scroll';
    }
  }

  send() {
    this._smsService.sendSms(this.message, this.chatId);
    this.message = null;
    this.scrollOnMessage();
  }

  call() {
    this._jsSip.handleOutgoingCall('', this.chatId);
  }

  isDiferentDate(actual: number, prev: number = Date.now()) {
    const actualDate = new Date(actual);
    const prevDate = new Date(prev);
    return actualDate.toDateString() !== prevDate.toDateString();
  }
}
