import { Injectable, Inject } from '@angular/core';

import JsSIP from 'jssip';
import { settings, CustomSettingsI } from './jssip.config';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import audioPlayer from './sounds.service';
import { ToneService } from './tone.service';
import { environment } from '../environments/environment';

@Injectable({ providedIn: 'root' })
export class JsSipService {
    private _audioElement: HTMLAudioElement;
    private _ua: any;
    public settings = settings;
    public environment = environment;
    public socket: any;
    public incomingSms = new BehaviorSubject(false);

    public state = {
        init            : false,
        status          : 'disconnected',
        session         : null,
        ringing         : false,
        incomingSession : null,
        autoanswer      : false
    };

    constructor(public toneService: ToneService) {
        audioPlayer.initialize();
        if (environment.production) {
            JsSIP.debug.disable('JsSIP:*');
        } else {
            JsSIP.debug.enable('JsSIP:*');
        }
    }

    setState(newState) {
        this.state = Object.assign({}, this.state, newState);
        return;
    }

    getState(state) {
        return this.state[state];
    }

    register() {
        if (this.state.status != 'registered') this._ua.register();
    }

    unregister() {
        if (this.state.status == 'registered') this._ua.unregister();

    }

    connect(userData) {
        if (!userData) {
            return;
        }

        // Start socket
        if (!this.socket) {
            this.socket = new JsSIP.WebSocketInterface(this.environment.socket.uri);
            if (this.environment.socket.via_transport !== 'auto') {
                this.socket.via_transport = this.environment.socket.via_transport;
            }
        } else {
            this.socket.disconnect();
        }

        if (this._ua && (this.state.status == "registered" || this.state.status == "connected")) {
            this._ua.unregister();
            delete this._ua;
        }

        // Setup JsSIP
        try {
            const reguri = userData.user + '@' + this.environment.URIDomain;
            this._ua = new JsSIP.UA({
                uri                 : reguri || this.settings.uri,
                password            : userData.password || this.settings.password,
                display_name        : userData.user || this.settings.display_name,
                sockets             : [ this.socket ],
                registrar_server    : this.settings.registrar_server,
                contact_uri         : this.settings.contact_uri,
                authorization_user  : this.settings.authorization_user,
                instance_id         : this.settings.instance_id,
                session_timers      : this.settings.session_timers,
                use_preloaded_route : this.settings.use_preloaded_route
            });

            // Add events to JsSIP
            this.addEvents(this._ua);

            // Start JsSIP
            this._ua.start();
            this.setState({init : true });

        } catch (error) {
            console.log('JsSIP config error', error);
            return;
        }
    }

    addEvents(sipUa) {
        sipUa.on('connecting', () =>
            this.setState({ status : 'connecting' }));

        sipUa.on('connected', () => {
            this.setState({ status: 'connected' });
            sipUa.register();
        });

        sipUa.on('disconnected', () =>
            this.setState({ status: 'disconnected' }));

        sipUa.on('registered', () =>
            this.setState({ status: 'registered' }));

        sipUa.on('unregistered', () => {
            const connected = (sipUa.isConnected()) ? 'connected' : 'disconnected';
            this.setState({ status:  connected});
        });

        sipUa.on('newMessage', (data) => {
            console.log('[SMS] - New Message', data);
            this.incomingSms.next(data);
        });

        sipUa.on('registrationFailed', (data) => {
            console.log('Registration Failed!');
            this.setState({ status: data.cause });
        });

        sipUa.on('newRTCSession', (data) => {
            if (data.originator === 'local') { return; } // Catch incoming actions only
            this.handleIncomingCall(data);
        });
    }

    /**
     * Handle incomming call events
     * @param data jsSip data session { session:object, incomingSession: object }
     */
    handleIncomingCall(data) {
        data.session.on('failed', (err) => {
            this.clearSessions();
            this.removeSounds();
        });

        data.session.on('ended', () => {
            this.clearSessions();
            this.removeSounds();
        });

        data.session.on('accepted', () => {
            this.toneService.stopRinging();
            this.setState({
                session         : data.session,
                incomingSession : null
            });
        });

        // Avoid if busy or other incoming
        if (this.state.session || this.state.incomingSession) {
            data.session.terminate({
                status_code   : 486,
                reason_phrase : 'Busy Here'
            });
            return;
        } else {
            // Start ringing and set the incoming session in the state
            this.incomingNotification(data);
            this.setState({ incomingSession: data });
        }

        if ( this.state.autoanswer === true ) {
            this.handleAnswerIncoming();
            this.setState({ autoanswer: false });
        }
    }

    handleOutgoingCall(uri, dtmfs: string) {

        if (this.state.status === 'Authentication Error' ||
            this.state.status === 'disconnected') {
            return -1;
        }
        // Get call method
        const callMethod = this.checkPrefixs(dtmfs, this.settings.custom);
        // Format uri
        const cs = this.settings.custom;
        switch (callMethod) {
            case 'virtual':     uri = `sip:${dtmfs}@${cs.defaultURIDomain}`; break;
            case 'conference':  uri = `sip:${dtmfs}@${cs.defaultURIDomain}`; break;
            case 'sip':         uri = dtmfs; break;
            case 'dtmfs':       uri = cs.dtmfsGateway; break;
            case 'outbound':    uri = `sip:${dtmfs}@${cs.defaultURIDomain};outbound=${cs.outbound}`; break;
            default:            uri = `sip:${dtmfs}@${cs.defaultURIDomain}`;
        }
        // Start session
        const session = this._ua.call(uri, {
            pcConfig             : this.settings.pcConfig || { iceServers: [] },
            mediaConstraints     : this.settings.call.mediaConstraints,
            rtcOfferConstraints  : this.settings.call.rtcOfferConstraints,
            sessionTimersExpires : 120
        });

        session.on('connecting', () => {
            // Don't play this "ringing" sound.
            // We may not even be ringing yet! Anyway, Let the remote send ringback.
            // this.toneService.startRinging();
            session.remote_identity.display_name = dtmfs;
            this.setState({ session });
        });

        session.on('failed', (data) => {
            this.removeSounds();
            let message: HTMLAudioElement;

            // Keep screen active while the error message is playing
            const addAudioEvent = (audio: HTMLAudioElement) => {
                const onAudioEnded = (event) => {
                    this.clearSessions();
                    event.target.removeEventListener('ended', onAudioEnded, false);
                    message = null;
                };
                audio.addEventListener('ended', onAudioEnded);
            };

            switch (data.cause) {
                case JsSIP.C.causes.AUTHENTICATION_ERROR:
                    console.log("Auth Error making call?")
                    break;
                case JsSIP.C.causes.NOT_FOUND:
                    message = audioPlayer.play('error_404');
                    addAudioEvent(message);
                    break;
                case JsSIP.C.causes.CANCELED:
                    message = audioPlayer.play('rejected');
                    addAudioEvent(message);
                    break;
                case JsSIP.C.causes.BUSY:
                    this.toneService.startBusyTone();
                    setTimeout(() => {
                        this.removeSounds();
                        this.clearSessions();
                    }, 5000);
                    break;
                default:
                    message = audioPlayer.play('error_general');
                    addAudioEvent(message);
            }

        });

        session.on('ended', () => {
            this.removeSounds();
            this.clearSessions();
            audioPlayer.play('hangup');
        });

        session.connection.onaddstream = (e) => {
            this.addStream(e);
        };

        session.connection.onremovestream = (e) => {
          this.removeSounds();
        };

        session.on('accepted', () => {
            // this.toneService.stopRinging();
            audioPlayer.play('answered');
            // If is call type is drmfs send tones after connect
            if (callMethod === 'dtmfs') {
                setTimeout(() => this.dtmfsCall(dtmfs, session), 2000);
            }
        });
    }

    handleAnswerIncoming() {
        this.state.incomingSession.session.answer(this.settings.answer);
        this.state.incomingSession.session.connection.onaddstream = this.addStream;
        this.state.incomingSession.session.connection.onremovestream = this.removeSounds;
    }

    handleRejectIncoming() {
       try {
            this.state.incomingSession.session.terminate({status_code: 486});
        } catch (error) {
            console.log('Session already finished');
        }
        this.clearSessions();
    }

    handleHangup() {
        try {
            this.state.session.terminate();
        } catch (error) {
            console.log('Session already finished');
        }
        this.removeSounds();
        this.clearSessions();
    }

    /**
     * Set all sessions state to null
     */
    clearSessions() {
        this.setState({
            session: null,
            incomingSession: null
        });
    }

    /**
     * Play audio stream
     * @param e Stream event
     */
    addStream(e) {
        this._audioElement = document.body.appendChild(document.createElement('audio'));
        this._audioElement.srcObject = e.stream;
        this._audioElement.play();
    }

    /**
     * Stop all sounds and remove audio elements
     */
    removeSounds() {
        // If is ringing
        this.toneService.stopAll();

        // If is playing a message
        audioPlayer.stopAll();

        // If an audio element exist
        if (this._audioElement) {
            document.body.removeChild(this._audioElement);
            this._audioElement = null;
        }
    }

    /**
     * Check what type of number is
     * @param phoneNumber Numbero or uri to call
     * @param prefixs settings.custom on jssip.config.ts
     */
    checkPrefixs (phoneNumber: string, cs: CustomSettingsI) {
        const isPrefix = (prefix: number) => {
            return phoneNumber.slice( 0, prefix.toString().length) === prefix.toString();
        };

        if      ( phoneNumber.includes('@') )                            { return 'sip'; }
        else if ( cs.virtualNumbersPrefixes.filter(isPrefix).length > 0)  { return 'virtual'; }
        else if ( cs.conferenceCallPrefixes.filter(isPrefix).length > 0)  { return 'conference'; }
        else if ( phoneNumber.length === 5 && cs.outbound)               { return 'outbound'; }
        else if ( cs.dtmfsGateway !== null )                             { return 'dtmfs'; }

        return 'standar';
    }

    /**
     * Send tones on jsSip session
     * @param dtmfs Tones to send
     * @param session jsSip session
     */
    dtmfsCall(dtmfs: string, session: any) {
        const tones = dtmfs + '#';
        let dtmfSender = null;
        if (session.connection.signalingState !== 'closed') {
            if (session.connection.getSenders) {
                dtmfSender = session.connection.getSenders()[0].dtmf;
            } else {
                const peerconnection = session.connection;
                const localStream = peerconnection.getLocalStreams()[0];
                dtmfSender = session.connection.createDTMFSender(localStream.getAudioTracks()[0]);
            }
            dtmfSender.insertDTMF(tones, 400, 50);
            console.log('Sending DTMF codes', tones);
        }
    }

    /**
     * Sends messages
     * @param messsage Message to send (string)
     * @param to Addres to send the message
    */
    sendMessage(message: string, to: string) {
        return new Promise((res, rej) => {
            // If SIP is not connected reject the action
            if (!this._ua.isConnected()) { rej({error: 'Not connected'}); return; }

            // Makes JsSip handle the promise responses.
            const eventHandlers = {
                succeeded: (data) => res(data),
                failed:    (data) => rej(data)
            };

            const options = {
               eventHandlers: eventHandlers
            };

            // Finaly, send the message.
            this._ua.sendMessage(to, message, options);
        });
    }

    /**
     * Self Generated Notification of incoming call
     * @param data Incoming rtc session
     */
    incomingNotification(data) {
        this.toneService.startRinging();
        if (document.hidden === true) {
            try {
                console.log('[SW] - Document is hidden - Sending push notification');
                navigator.serviceWorker.getRegistration()
                    .then( (registration: any) => {
                        const a = registration.showNotification('Webph.one - Incoming call', {
                            body: data.session.remote_identity.display_name,
                            vibrate: [200, 100, 200, 100, 200, 100, 400],
                            tag: 'document-hidden',
                            icon: 'assets/icons/android-chrome-192x192.png',
                            actions: [
                            { action: 'yes', title: 'Answer' },
                            { action: 'no', title: 'Hang up' }
                            ]
                        });
                    });
            } catch (error) {
                console.log('NOTIFICATION ERROR', error);
            }
        }
        return;
    }

}
