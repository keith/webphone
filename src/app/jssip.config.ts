export const settings = {
    display_name        : 'webrtc',
    uri                 : 'webrtc@rhizortc.specialstories.org',
    password            : 'verysecret',
    socket              :
    {
        uri           : 'wss://rhizortc.specialstories.org:8443',
        via_transport : 'auto',
    },
    registrar_server    : null,
    contact_uri         : null,
    authorization_user  : null,
    instance_id         : null,
    session_timers      : true,
    use_preloaded_route : false,
    pcConfig            :
    {
        rtcpMuxPolicy : 'negotiate',
        iceServers    :  []
    },
    answer:
    {
        mediaConstraints: {
            audio: true,
            video: false
        },
        rtcOfferConstraints : {
            offerToReceiveAudio : true,
            offerToReceiveVideo : false
        }
    },
    call:
    {
        mediaConstraints: {
            audio: true,
            video: false
        },
        rtcOfferConstraints : {
            offerToReceiveAudio : true,
            offerToReceiveVideo : false
        }
    },
    custom:
    {
        dtmfsGateway: null,
        outbound: null,
        defaultURIDomain: 'rhizortc.specialstories.org',
        // The below does not really matter 'virtual' or 'default' has the same uri
        virtualNumbersPrefixes: [999100, 999200, 505050, 505051, 999800, 505052],
        virtualNumberPrefix: 505050,
        conferenceCallPrefixes: [500],
        fakeEmail: '@development.tic-ac.org'
    }
};

export interface CustomSettingsI {
    dtmfsGateway?: string;
    outbound?: string;
    defaultURIDomain: string;
    virtualNumbersPrefixes: number[];
    virtualNumberPrefix: number;
    fakeEmail: string;
    conferenceCallPrefixes: number[];
}
