import { Component, OnInit, OnDestroy } from '@angular/core';
import { UserService, UserI } from '../user.service';
import { FormGroup, FormControl } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.scss']
})
export class AccountComponent implements OnInit, OnDestroy {
  user: UserI;
  usersub: any;
  account = new FormGroup({
    user: new FormControl(''),
    password: new FormControl('')
  });

  constructor(
    private userService: UserService,
    private router: Router,
  ) {
      this.userService.isUser().then(() => {
	      this.usersub = this.userService.userData().subscribe(data => {
             this.user = data;
	           console.debug(this.user);
	      });
	      this.account.setValue({ user: this.user.user, password: this.user.password });
      });
  }

  ngOnInit() {
  	console.debug('Account Init', this.usersub);
  }

  ngOnDestroy() {
   	console.debug('Account Destroy');
   	if (this.usersub) { this.usersub.unsubscribe(); }
  }

  submit() {
  	this.user.user = this.account.value.user;
  	this.user.password = this.account.value.password;
  	this.userService.update(this.user);
    this.router.navigate(['/directory'])
  }
}
