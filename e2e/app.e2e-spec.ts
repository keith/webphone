import { SaycelSipAppPage } from './app.po';

describe('TicAc-sip App', () => {
  let page: SaycelSipAppPage;

  beforeEach(() => {
    page = new SaycelSipAppPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
